FROM node:8

WORKDIR /usr/src/app/frontend
COPY serve.js ./
COPY build ./

COPY . .